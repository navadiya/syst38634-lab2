package studentRegistration;
/* Deep Navadiya
 * 991550374
 */
import static org.junit.Assert.*;

import org.junit.Test;

import Time.Time;

public class TestStudent {

	@Test
	public void testCheckPasswordLengthRegular() {
		boolean passLength= Student.checkPasswordLength("abcdefghi");
		assertTrue("Invalid password Length",passLength == true);
	}

	@Test
	public void testCheckPasswordLengthException(){
		boolean passLength= Student.checkPasswordLength("abcdef");
		assertFalse("Invalid password Length",passLength);
	}
	
	@Test
	public void testCheckPasswordLengthBoundaryIn(){
		boolean passLength= Student.checkPasswordLength("abcdefgh");
		assertTrue("Invalid password Length",passLength == true);
	}
	
	@Test
	public void testCheckPasswordLengthBoundaryOut(){
		boolean passLength= Student.checkPasswordLength("abcdefghijk");
		assertTrue("Invalid password Length",passLength);
	}
	
	@Test
	public void testisValidPass() {
		boolean passValid= Student.isValidPass("abcdef98");
		assertTrue("Password does not include digit.",passValid == true);
	}
	
	@Test
	public void testisValidPassException(){
		boolean passValid= Student.isValidPass("abcdefgh");
		assertFalse("Password does not include digit.",passValid);
	}
	
	@Test
	public void testisValidPassBoundaryIn(){
		boolean passValid= Student.isValidPass("abcde898");
		assertTrue("Password does not include digit.",passValid == true);
	}
	
	@Test
	public void testisValidPassBoundaryOut(){
		boolean passValid= Student.isValidPass("98989898");
		assertTrue("Password does not include digit.",passValid);
	}
}
