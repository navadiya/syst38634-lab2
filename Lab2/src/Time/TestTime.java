package Time;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestTime {

	@Test
	public void testGetMilliseconds(){
		int totalMilliseconds= Time.getMilliseconds("12:05:05:05");
		assertTrue("Invalid number of Milliseconds",totalMilliseconds==5);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetMillisecondsException(){
		int totalSeconds= Time.getMilliseconds("01:01:01:0A");
		fail("Invalid number of Milliseconds");
	}
	
	@Test
	public void testGetMilliSecondsBoundaryIn(){
		int totalSeconds= Time.getMilliseconds("00:00:00:999");
		assertTrue("Invalid number of Milliseconds", totalSeconds == 999);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetMilliSecondsBoundaryOut(){
		int totalSeconds= Time.getTotalSeconds("01:01:01:1000");
		fail("Invalid number of Milliseconds");
	}
	
	@Test
	public void getTotalSecondsRegular(){
		int totalSeconds= Time.getTotalSeconds("01:01:01");
		assertTrue("The Time does not match the results",totalSeconds == 3661);
	}
	
	@Test (expected=NumberFormatException.class)
	public void getTotalSecondsException(){
		int totalSeconds= Time.getTotalSeconds("01:01:0A");
		fail(" The Time provided is not valid");
	}
	
	@Test (expected=NumberFormatException.class)
	public void getTotalSecondsBoundaryOut(){
		int totalSeconds= Time.getTotalSeconds("01:01:60");
		fail("The Time provided is not valid");
	}
	
	@Test
	public void getTotalSecondsBoundaryIn(){
		int totalSeconds= Time.getTotalSeconds("00:00:59");
		assertTrue("The Time provided does not match the results", totalSeconds == 59);
	}

}
